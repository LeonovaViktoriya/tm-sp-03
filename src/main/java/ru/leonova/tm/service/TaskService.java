package ru.leonova.tm.service;

import org.jetbrains.annotations.NotNull;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import ru.leonova.tm.entity.Project;
import ru.leonova.tm.entity.Task;
import ru.leonova.tm.repository.ITaskRepository;

import java.util.List;

@Service
@Transactional
public class TaskService implements ITaskService {

    @Autowired
    private ITaskRepository itaskRepository;

    @Override
    @Transactional
    public List<Task> findAll() {
        return itaskRepository.findAll();
    }

    @Override
    public Task findOneById(@NotNull final String id) throws Exception {
        if(id.isEmpty()) throw new Exception("id task is empty!");
        return itaskRepository.getOne(id);
    }

    @Override
    public void removeTask(@NotNull final Task task) {
        itaskRepository.delete(task);
    }

    @Override
    public void save(@NotNull final Task task) {
        itaskRepository.save(task);
    }

    @Override
    public List<Task> findAllByProjectId(@NotNull final String id) throws Exception {
        if(id.isEmpty()) throw new Exception("id project is empty!");
        return itaskRepository.findAllByProject_ProjectId(id);
    }

    @Override
    public void removeAllByProjectId(@NotNull final String id) throws Exception {
        if(id.isEmpty()) throw new Exception("id project is empty!");
        itaskRepository.deleteAllByProject_ProjectId(id);
    }

    @Transactional
    @Override
    public void update(@NotNull final Task task) {
        if (task.getTaskId().isEmpty() || task.getName().isEmpty() || task.getDescription() == null || task.getDescription().isEmpty()) {
            throw new IllegalArgumentException();
        }
        itaskRepository.update(task.getTaskId(), task.getName(), task.getDescription(), task.getDateStart(), task.getDateEnd(), task.getStatus());
    }
}
