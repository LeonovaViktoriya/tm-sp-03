package ru.leonova.tm.service;

import org.jetbrains.annotations.NotNull;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import ru.leonova.tm.entity.Project;
import ru.leonova.tm.repository.IProjectRepository;

import java.util.List;

@Service
@Transactional
public class ProjectService implements IProjectService {

    @Autowired
    private IProjectRepository iprojectRepository;

    @Override
    @Transactional
    public List<Project> findAll() {
        return iprojectRepository.findAll();
    }

    @Override
    public Project findOneById(@NotNull final String projectId) throws Exception {
        if(projectId.isEmpty()) throw new Exception("id project is empty!");
        return iprojectRepository.getOne(projectId);
    }

    @Override
    public void save(@NotNull final Project project) {
        iprojectRepository.save(project);
    }

    @Override
    public void removeProject(@NotNull final Project project) {
        iprojectRepository.delete(project);
    }

    @Transactional
    @Override
    public void update(@NotNull final Project project) {
        if (project.getProjectId().isEmpty() || project.getName().isEmpty() || project.getDescription() == null || project.getDescription().isEmpty()) {
            throw new IllegalArgumentException();
        }
        iprojectRepository.update(project.getProjectId(), project.getName(), project.getDescription(), project.getDateStart(), project.getDateEnd(), project.getStatus());
    }
}
