package ru.leonova.tm.entity;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.hibernate.annotations.CacheConcurrencyStrategy;
import org.springframework.format.annotation.DateTimeFormat;
import ru.leonova.tm.enumerated.Status;

import javax.persistence.*;
import java.io.Serializable;
import java.util.Date;

@Getter
@Setter
@NoArgsConstructor
@Entity
@org.hibernate.annotations.Cache(usage = CacheConcurrencyStrategy.READ_WRITE, region = "model")
@Cacheable
@Table(name = "app_task")
public final class Task implements Serializable {

    @Id
    @Column(name = "id")
    private String taskId;
    private String name;
    private String description;
    @Column(name = "createDate")
    @DateTimeFormat(pattern = "dd.mm.yyyy")
    private Date dateSystem;
    @Column(name = "beginDate")
    @DateTimeFormat(pattern = "dd.mm.yyyy")
    private Date dateStart;
    @Column(name = "endDate")
    @DateTimeFormat(pattern = "dd.mm.yyyy")
    private Date dateEnd;
    @Column(name = "statusType")
    @Enumerated(EnumType.STRING)
    private Status status;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "project_id")
    private Project project;

}

