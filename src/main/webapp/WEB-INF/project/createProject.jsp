<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>

<html>
<head>
    <style type="text/css">
        div.form-row {
            margin: 10px;
            width: 30%;
        }

        input.btn {
            margin: 10px;
            width: 5%;
        }
    </style>
    <link rel="stylesheet"
          href="http://maxcdn.bootstrapcdn.com/bootstrap/4.4.1/css/bootstrap.min.css">
    <title>Edit project</title>
</head>
<body>
    <nav class="navbar navbar-dark bg-dark">
        <a class="navbar-brand">Create project</a>
    </nav>
    <c:url value="/projects/add" var="var"/>
    <form:form method="POST" action="/projects/add" modelAttribute="project">
        <div class="form-row">
            <label for="name1">Name</label>
            <input type="text" name="name1" id="name1" class="form-control">
        </div>
        <div class="form-row">
            <label for="description1">Description</label>
            <input type="text" name="description1" id="description1" class="form-control">
        </div>
        <div class="form-row">
            <label for="dateStart1">date Start</label>
            <input type="text" name="dateStart1" id="dateStart1" class="form-control">
        </div>
        <div class="form-row">
            <label for="dateEnd1">date End</label>
            <input type="text" name="dateEnd1" id="dateEnd1" class="form-control">
        </div>

        <div class="form-row">
            <label for="status">Выберите status</label>
            <form:select path="status" id="status" name="status" class="form-control">
                <c:forEach items="${statusList}" var="status" >
                    <option id="status" name="status" value=${status}>${status}</option>
                </c:forEach>
            </form:select>
        </div>
        <input type="submit" value="Add" class="btn btn-success">
    </form:form>
</body>
</html>